package csvdelimiter

import "strings"

func newParser(delimiters runeDict) *parser {
	return &parser{
		scores: make(scores),
		delimiters: func(d runeDict) runeDict {
			if len(d) > 0 {
				return d
			}
			return DefaultDelimiters
		}(delimiters),
	}
}

type parser struct {
	delimiters runeDict
	inQuotes   bool
	escape     bool
	last       rune
	scores
	builder strings.Builder
}

func (p *parser) feed(r rune) {
	switch {
	case p.escape:
		p.escape = false
	case p.delimiters.includes(r):
		// if delimiter next to and outside of '"' - give bonus
		if p.last == quote && !p.inQuotes {
			p.scores[r] += 2
		} else if !p.inQuotes {
			p.scores[r]++
		} else {
			p.scores[r] += 0.5
		}
	case r == quote:
		if !p.inQuotes && p.delimiters.includes(p.last) {
			p.scores[p.last]++
		}
		p.inQuotes = !p.inQuotes
	case r == escape:
		p.escape = true
	}

	if !p.escape {
		p.last = r
	}

	p.builder.WriteRune(r)
}
