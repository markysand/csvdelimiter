[![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white&style=flat-square)](https://pkg.go.dev/codeberg.org/markysand/csvdelimiter)

# Package csvdelimiter

    go get "codeberg.org/markysand/csvdelimiter"

Package csvdelimiter is a simple csv delimiter detector package that works
with io.Reader inputs.

Package csvdelimiter uses a rudimentary algorithm for detecting the delimiter, searching for the most frequently used letter out of a subset of approved delimiter letters. Extra weight is added depending on how the delimiter relates to citation marks.

For most use cases it should not fail.
