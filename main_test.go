package csvdelimiter

import (
	"io"
	"strings"
	"testing"
)

// cSpell:disable
var testCsv1 = `"jhg";"splop";"bladabadoom";"badoom";"footos";"grooop";"klendathu"`
var testCsv2 = `abra,kad;abra,simsa;labim,saladu,sala;dim,salab;adam,abra,kada;bra,si;msal;abim,saladu,salad;im`
var testCsv3 = `"abra",kad;abra,"simsa;labim",saladu,sala;dim,"salab;adam",abra,"kada;bra","si;msal;abim",saladu,salad;im`
var testCsv4 = `abra;kadavra,badoom,badam;spleep,yoghurt,creeky;"kabaam";pakka;"blam";"bloom";bim`
var testCsv5 = `heycon,beycon,plejcon`

// cSpell:enable
func TestDetect(t *testing.T) {
	type args struct {
		r   io.Reader
		del []rune
	}
	tests := []struct {
		name    string
		args    args
		want    rune
		wantErr bool
	}{
		{
			name:    "Quoted, default delimiter setup",
			args:    args{r: strings.NewReader(testCsv1)},
			want:    ';',
			wantErr: false,
		},
		{
			name: "Quoted, semicolon delimited, custom delimiter",
			args: args{r: strings.NewReader(testCsv1),
				del: []rune{';'}},
			want:    ';',
			wantErr: false,
		},
		{
			name: "Quoted, semicolon delimited, invalid delimiter",
			args: args{r: strings.NewReader(testCsv1),
				del: []rune{'*'}},
			want:    emptyRune,
			wantErr: true,
		},
		{
			name: "Confusing csv",
			args: args{r: strings.NewReader(testCsv2),
				del: []rune{',', ';'}},
			want:    emptyRune,
			wantErr: true,
		},
		{
			name: "Confusing csv but quote adjacency clears it up",
			args: args{r: strings.NewReader(testCsv3),
				del: []rune{',', ';', '*'}},
			want:    ',',
			wantErr: false,
		},
		{
			name: "Confusing csv, almost too hard",
			args: args{r: strings.NewReader(testCsv4),
				del: []rune{',', ';', '*'}},
			want:    ';',
			wantErr: false,
		},
		{
			name:    "Too short csv",
			args:    args{r: strings.NewReader(testCsv5)},
			want:    emptyRune,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, _, err := detect(tt.args.r, tt.args.del...)
			if (err != nil) != tt.wantErr {
				t.Errorf("Detect() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Detect() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDetect_Reader(t *testing.T) {
	_, rd, _ := detect(strings.NewReader(testCsv1))

	var b = new(strings.Builder)

	_, err := io.Copy(b, rd)

	if err != nil {
		t.Fatal(err)
	}

	if result := b.String(); result != testCsv1 {
		t.Fatalf("Failed reader to writer, expected\n\n%v\n\ngot\n\n%v\n\n", testCsv1, result)
	}
}

func Test_runeDict_includes(t *testing.T) {
	type args struct {
		r rune
	}
	tests := []struct {
		name string
		rd   runeDict
		args args
		want bool
	}{
		{
			"Semicolons",
			runeDict{';', ',', '/'},
			args{';'},
			true,
		}, {
			"Semicolons - fail",
			runeDict{'*', ',', '/'},
			args{';'},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.rd.includes(tt.args.r); got != tt.want {
				t.Errorf("runeDict.includes() = %v, want %v", got, tt.want)
			}
		})
	}
}
