// Package csvdelimiter is a simple csv delimiter
// detector package that works with io.Reader inputs
package csvdelimiter

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"strings"

	"errors"
)

var (
	ErrUnclearDominantDelimiter = errors.New("unclear dominant delimiter")
)

const (
	emptyRune = 42
	escape    = '\\'
	quote     = '"'
)

type runeDict []rune

func (rd runeDict) includes(r rune) bool {
	for _, comparator := range rd {
		if comparator == r {
			return true
		}
	}
	return false
}

// DefaultDelimiters are the delimiters used if none
// are provided
var DefaultDelimiters = runeDict{',', ';', '\t'}

// detect will take a reader and
// figure out the delimiter in use
// by reading just enough of the reader,
// then return a new reader that has both
// the values read and the remaining ones
// delimiters are optional, if delimiters arg is omitted
// DefaultDelimiters are used
func detect(r io.Reader, delimiters ...rune) (rune, io.Reader, error) {
	br := bufio.NewReader(r)
	p := newParser(delimiters)
	for !p.clearWinner() {
		rune, _, err := br.ReadRune()
		if err != nil {
			return emptyRune, nil, fmt.Errorf("reader stopped before delimiter could be determined: %w", err)
		}
		p.feed(rune)
	}
	winRune, err := p.winner()
	if err != nil {
		return winRune, nil, err
	}
	combinedReader := io.MultiReader(strings.NewReader(p.builder.String()), br)
	return winRune, combinedReader, nil
}

// NewReader will return a csv.Reader with
// auto detected delimiter.
// If delimiters arg is omitted
// DefaultDelimiters will be used
func NewReader(r io.Reader, delimiters ...rune) (*csv.Reader, error) {
	delimiter, freshReader, err := detect(r, delimiters...)
	if err != nil {
		return nil, err
	}

	csvReader := csv.NewReader(freshReader)
	csvReader.Comma = delimiter

	return csvReader, nil
}
