package csvdelimiter

import (
	"fmt"
	"sort"
	"strings"
)

type scores map[rune]float64

func (s scores) winner() (rune, error) {
	var (
		winner   rune = emptyRune
		maxScore float64
	)
	for key, value := range s {
		if value > maxScore {
			winner, maxScore = key, value
		}
	}
	if maxScore > 0 {
		return winner, nil
	}
	return winner, ErrUnclearDominantDelimiter
}

func (s scores) clearWinner() bool {
	l := len(s)
	if l == 0 {
		return false
	}
	subScores := make([]float64, l)
	var i int
	for _, val := range s {
		subScores[i] = val
		i++
	}

	sort.Slice(subScores, func(i, j int) bool {
		return subScores[i] > subScores[j]
	})
	var (
		first  = subScores[0]
		second float64
	)
	if l > 1 {
		second = subScores[1]
	}
	return first > second*2+3
}

func (s scores) String() string {
	var b strings.Builder
	for key, value := range s {
		b.WriteString(fmt.Sprintf("Rune %q - score %v\n", key, value))
	}
	return b.String()
}
